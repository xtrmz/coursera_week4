# Annexe 1 - Memo/mail week 3

```
Dear -VP of Direction of Digital Data-

my name is Luc and I work since 2000 in French Telecom company. First as a developper, then as ScrumMaster and now I am working with Operation teams as project manager.
I work on a big eCommerce platform which involves several engineers (developpers, opérations, servicedesk).

Every release, we faced with difficulty to set up a new **working** environnement for a major web site.
The lifecycle of a release is about 3 months (developpment and deployment).
The deployment effort and configuration for a new release costs 2 man / weeks including :
   - staging environment installation
   - written timelined production operations procedures  
   - handoffs between developpers and operations, handoffs between operations and service desk
   - many non-automatic operations.

Production deployment spends one night each month, any rollback is very painful.


We could do better by using DevOps principles. Behind this buzzword, the will of team members is to deliver more value and faster to our customer.
    - Delivering small changes often and faster using automatisation.
    - Working together with sharing the same objective.
This will improve our deployment process.

For this, we could use the three ways model :
    * observing our system with a particular focus on all business value streams : identifying handoffs, delays during the deployment phase.
    * experiment and learn : find, test, adjust and validate improvements quickly
    * amplifying feedback loops : taking time to improve the process, sharing practices and shared documentations.
We have to find defects and fixed them before deploying a new release. Adding automatic tests before the release will increase quality and then the team trust, and will secure the deployment .

At each deployment, we will improve it, by doing the PDCA. It aims :
* to plan how we could deploy the website
* to do it - deploy it with the new operations
* to check it, if is there some gaps, verify if it works well
* to adjust the process of deploymentand loop ... .
Automation of the web site deployment has to be our goal using a deployment pipeline.
We will reduce cycle time of website deployment and tends to on-demand release deployment. We could do this by sharing objectives : **deploying each release in 2 hours**.
Merging taskboards of each team (developpment, operation) will break silos. People will focus to the shared objective. Collaboration between teams (operations and developpment) has to be the key human factor of our success.

Would you be interested in a one hour briefing to discuss the matter further ?

Regards,

--Luc

```

# Annexe 2 - Value Stream Mapping exercice

## Identify and propose a list of key team members necessary to actually create the map, don’t forget your leaders.

* a product manager
* a representant of desktop team development
* a representant of mobile team development
* a system administrator
* manager of the developers
* manager of the system administrators
* tests/QA actor
* the release manager
* CAB
* Support Team

## Write an explanation that you want to deliver to this group to create a shared understanding of lean and how it applies to software delivery.

Sometimes we think that we know all how it works, but there is some details that we could missed.
Theses details could be :

  * misunderstood (process of delivery, new functionnalities...)
  * different priorities between the actors
  * not the same objectives : adding new functionnalities, keep the infrastructure up and running
  * many handoffs between teams

With the value stream mapping, we want to observe, mesure and extract the reality.
We have to analyse each step of the delivering chain. What is going on ? What is blocked ? How many times ?
Where are the barriers to the flow ? We will see the overall process.

Lean could help with their tools (pdca, kaizen, value stream mapping), but one of lean pillars is : respect for people.

That's why operators in the deliver chain will participate:

  * by mesuring and documenting all the steps
  * then thinking and proposing some changes for solving and reducing wastes.
  * and finaly learn how to improve the overall processus.

Value Stream Mapping could improve the overall process so keyleaders are here to give/help teams to create the information map.

## Document the step-by-step process to ship a typical feature in your organization.

### Planning phase
A new release is planned, there will be one major feature on the web site and the mobile application.
After being funded, the functionality will have to be developed.
This feature is explained first to the desktop development team and then to the mobile development team.

### Development phase
Then the development could be begin.
The desktop development team made a branch from latest stable release and begin the new fonctionnality.
The mobile development team made the same.

### Tests
The same team perfoms mobile and desktop testing. Once finished, the branches are ready to merge.

#### Mobile Tests
All mobile phones are not always available. So team has to wait. There are no automatic tests for this step.

#### Desktop Tests
The entire functional scope is not covered by automatic tests. So there are so manual testing steps.

### Merge phase
This is a difficult step. We have to merge some bugfixes, mobile and desktop development of the functionnality.
Bugfixes step are not in the value stream mapping.

2 developers on each development team works together to embbed all changes.
The bugfixes are merged by these two developers

### Integration Tests, Rework phase
Integration team has to install in staging environment.
After this, tests are made, some rework has to be done.
There are many round trip between tests and development teams before the release step.

### Releasing
It consists in two part :

  * tag the release, build the artifact
  * check all prerequisite for the production ready state.
A planning has to be fixed.

The release could be planned for deploy and respect all the prerequisites (this step is an ITIL part before our process deployment).

### Deployment phase
The deployment phase will be proceed by an system administrator and a tester will be mobilized for doing smoke tests.
The deployment phase has many manual actions.

## Provide estimates of how long each step takes, including the true process time along with any wait time.

The time estimation of each step figures in the value stream mapping (current state).

![current state](week4_initial_state.png)

## Once you have all of the steps and times captured, document your ideal future state which almost always includes removing steps and reducing the total wait time.

This is the ideal future state. For trying to obtain this result, we will build a cross-functionnal team with mobile developers, desktop developpers and system administrator.

![future ideal state](week4_futur_ideal_state.png)

Only one planning meeting with all involved people (desktop devs, mobile devs and sysops).
We will :

  * write automatic tests
  * write an automatic build which permit to release in one command
  * using a provisionner for the automatic deployment of the application.

To do this, we will share a single backlog where all items will be visible.
By putting some wip limits in our kanban, theses limits will contributes to better focus, clearer communication. So, we will have more realistic analysis and projection.

With only one team, we don't need to merge anything.

Automatic tests will be writen with help of Integration team. Integration team will take less time to validate the software.

Automatic deployment will be perform with a continuous deployment tool and a provisionner (ansible or other).
Automatic deployment could be proceed by the tester, with single action button.

We will thus reduce the lead time by a little less than half.
