# Slide - Embrace devops

there is no alternative, we have to **learn fast**

# Slide - our priorities

* Reacting quickly to customer demand
* Achieving a faster speed to market

> Objectives : Total customer satisfaction

# Slide - observation
* Development and deployment cycle are too slow
* Poor quality level
* Unsatisfied customer
* Employees are demotivated

# Slide - principles

* Automation
* Short feedback loops
* Culture of learning
* Respect for people

# Slide - principles

# Slide - stop

* Multitasking
* Blaming
* Local optimizations
* Local objectives

# Slide 4 - start

* Encouraging innovations and experimentations
* Observe the reality
* Making work visible
* Share our experience
* Automate everything

# Slide - try
* Cross-functionnal team
* Shift from big batch to smaller batch
* Focus on speed
* Progressives improvements

# Slides Tools and practices
* Kanban with WIP Limits
* Motivate people with blameless incident reviews
* Problem solving with PDCA
* Setting up experimentation with A/B Testing
* Observe/Surface the reality with Value Stream Mapping. It will help identify bottlenecks.
* One day retrospective

# Last slide
We have to change to be the :
    "Smart motivated people working together"

for facing new challenges, helping new customers...

# Question slide
