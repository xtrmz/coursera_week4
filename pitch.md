# Slide - Embrace devops
I appreciate that you let me detail information provided in my email.

Today, we have to turn the page on our habits.
Indeed, in order to succeed in the challenges of tomorrow, we must transform existing cultures into new ones and offer innovative solutions to our customers.
The digital transformation we want to offer to our customers have to first address of all be done through our self transformation.
Above all through the transformation of people.
We don't have fear to embrace change, we must learn and learn quickly.

# Slide - our priorities

We must respond quickly to our customers' requests, being time-to-market, anticipate with one objective:
   entire satisfaction of our customers

That is why devops culture and devops principles will help us to meet theses challenges.
I recommend us to adopt the DevOps model into our organization.
DevOps is not only tooling, special practices, it's about humans: better communication and collaboration between teams.
Practices and patterns that turn human capital into high-performance organizational capital: increasing reliability, resilience, security on our system.
Quality at all levels of our software production chain, speed through the automation of our processes but before tight-knit teams who want to learn how to improve.
It is not a simple promise to break down the barriers between developpers and systems operators but it is a reel challenge to change culture and flexibility for us and our customers.

# Slide - observation
Today, our development and deployment cycles are too slow. Quality is not there.
Hand-offs between requirements definition, development phase, test phase and operations results a very slow lifecycle. The deployment effort and configuration for a new release is too heavy. Any rollback is very painful

Customers are not satisfied with our products. And finally, employees are demotivated.

Too many bugs in out production systems.
Dealing with unsatisfied customers.
Turnover increases, our employees are demotivated !

# Slide - principles
We need to set up automation chains, from continous integration to continous deployment and continous delivery.
With the three ways models :

  * System thinking : focus to emphase the performance of the entire system
  * Amplifying feedback loops
  * and a culture of continuous experimentation and learnin (with failing fast, experimenting, sharing issues and conclusion).

Learn from our mistakes and successes through short feedback loops.
We have to learn, and learn how to learn.

All this with respect for our employees.

# Slide - stop

Stop multitasking (too many work in progress)

Stop overproduction (too many over-engineering, too complex architecture, YAGNI - You Aren't Gonna Need It)

Stop blaming, encouraging, giving help arrêter de punir les gens, mais les encourager et les aider

Stop local optimizations, sub-optimizing globally, because we could not see global constraints.

Same for objectives, stop local objectives. We must have a global approach, customer oriented.

# Slide - start

We must encourage innovation and the innovative spirit of our employees.

We have to start doing experiments.

We have to start to surface the reality (VSM), observe and try little improvments.

We have to making work visible for a better sharing, handling competing priorities, advocating better communication and collaboration between units. Making work visible in order to help teams better understand the workflow.

We have to start sharing our experiences across the team and directions, our practices, our backlog, our documentations. Encourage bridging between teams.

We have to automate everything, this will remove repetitive task and human error.

# Slide - try

* Cross-functionnal team
* Shift from big batch to smaller batch
* Focus on speed
* Progressives improvements

Without Cross-functionality and flexibility, DevOps approach just won’t work !
The cross-functionnal team will covered all scope of the software development and delivery, and improve it.
> You build it, you run it, you improve it

Adopt new way of developping software, focusing on customer services, turning monolith to small batches by exposing API and turning our complex system to a loosely coupled architecture.

We have to focus on speed, keeping the quality-in. Focusing on Speed is not a trade between quality or stability. It is a way to improve quickly our process (avoiding rework, defects).

With short cycles (iteration, ...), we could made many progressive improvements that conducts to better results.


# Slides Tools and practices

Here is a list of tools and practices, we will set up quickly in our production systems.

* Kanban with WIP Limits (limiting the WIP and focus to achieve the work (the work has to be done !)).
* Motivate people with blameless incident reviews
* Problem solving with PDCA (A3 problem solving)
* Setting up experimentation with A/B Testing
* Observe/Surface the reality with Value Stream Mapping. It will help identify bottlenecks.
* One day retrospective (5 minutes daily questions : an effective tool to close feedback loops, initiate change and establish a culture of relentless improvement).

# Slides Actions
Here is some concret actions that will move to a devops culture :

* transforming one team in a cross-functionnal Team
* Start sharing a product backlog
* Reduce iteration time with one day retrospective (for reduce feedback cycle)
* Surface Reality with the contination of the previous Value Stream Mapping
* increase automation with CI/CD

# Last slide
That why our organization have to support and promote the mechanics of the DevOps approach.

We have to change to be the :
    "Smart motivated people working together"

for facing new challenges, helping new customers...

# Question slide
Any questions ?

Q: What next ?

A: I propose the creation of two autonomous crossfonctionnal teams.
One of the teams is the one of the future ideal state of the Value Stream Mapping exercice.
These teal would be responsible for developing mobile/desktop developments, testing these features and delivering them to production.
The other team would be in charge of a tooling-oriented activity to help with the rapid implementation of tools for sharing and automation.


